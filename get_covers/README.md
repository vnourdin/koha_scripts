# get_covers

english below

## FR

Ces scripts servent à télécharger et préparer des images de couvertures pour un import dans Koha.\
Pour l'instant, le script automatique se base sur Decitre, mais si vous avez une source d'images plus appropriée à votre besoin, faites-m'en part et je pourrais facilement adapter le script!

#### Fonctionnement

Après avoir exporté la liste des éléments sans couverture, un premier script va tenter de **récupérer les couvertures automatiquement depuis le site de [Decitre](https://www.decitre.fr)**.\
Il génère un fichier zip prêt à être importé dans Koha, et un fichier csv contenant la liste des éléments non trouvés, afin de faire la recherche de couvertures (URL) manuellement.\
Une fois la recherche manuelle effectuée, un deuxième script télécharge toutes les images et crée un zip à importer.

#### Marche à suivre

1. Depuis Koha, créer un rapport SQL avec la requête suivante :
    ```
	SELECT biblioitems.biblionumber,
	    biblioitems.isbn,
	    biblio.title
	FROM biblioitems
		JOIN biblio
		ON biblioitems.biblionumber = biblio.biblionumber
	WHERE biblioitems.biblionumber NOT IN (
	  SELECT biblionumber
	  FROM biblioimages
	)
    ```
    Ce rapport fournit la liste des tous les éléments qui n'ont pas d'image associée.
1. Exécuter le rapport puis le télécharger au format csv\
	![Téléchargement CSV](export_fr.jpg "Téléchargement CSV")
1. Lancer le script *decitre* avec comme argument le chemin vers le fichier téléchargé :\
    `./get_covers_from_decitre.sh /home/user/.../mon_fichier.csv`\
    Le script va afficher, pour chaque ligne en entrée, s'il a trouvé une image de couverture ou non.
1. Chercher manuelemment les URL des images pour les éléments du fichier not_found_decitre.csv :\
    On remplace le titre par un lien vers une image de couverture trouvée sur internet par exemple\
    `271;"Le grand livre des énigmes"`\
    devient\
    `271;https://images.epagine.fr/323/9782501105323_1_75.jpg`\
    **Attention** le liens doit se terminer par un fichier .jpg ou .png (par exemple), s'il ne termine pas par un nom de fichier, le script ne pourra pas le récupérer.
1. Lancer le script *urls* avec comme argument le chemin vers le fichier créé :\
    `./get_covers_from_urls.sh /home/user/.../fichier_fait_main.csv`\
    Le script va afficher, pour chaque ligne en entrée, s'il a trouvé une image de couverture ou non.
1. Retourner sur Koha, dans outils > Télécharger des images de couverture locales
1. Sélectionner le zip "covers_from_decitre.zip" généré à côté du script.
1. Laiser coché "Fichier Zip" puis cliquer sur "Traitement des images"
1. Sélectionner le zip "covers_from_urls.zip" généré à côté du script.
1. Laiser coché "Fichier Zip" puis cliquer sur "Traitement des images"

## EN

Those scripts allow to download and package cover images to be imported into Koha.\
For now, it use the french bookstore Decitre to retrieve images, but if you have another source that match better your needs, please ask me and I could adapt my script.

#### Behavior

After exporting a list of every items without image, a first script will **try to download cover images from the [Decitre](https://www.decitre.fr) (french bookstore) website.**\
It generate a zip file ready to be sent to Koha, and a csv file containing elements for which no cover were found, to let you do the job manually.\
Once you research for cover images URL done, a second script handle the downloading and packaging to make a readu to import zip.

#### HowTo

1. From Koha, create a SQL report with following query:
    ```
	SELECT biblioitems.biblionumber,
	    biblioitems.isbn,
	    biblio.title
	FROM biblioitems
		JOIN biblio
		ON biblioitems.biblionumber = biblio.biblionumber
	WHERE biblioitems.biblionumber NOT IN (
	  SELECT biblionumber
	  FROM biblioimages
	)
    ```
    This report give a list of every items without any image.
1. Run the report then download it as CSV\
	![Download CSV](export_en.jpg "Download CSV")
1. Run the script *decitre* with the path to downloaded file as arg:\
    `./get_covers_from_decitre.sh /home/user/.../my_file.csv`\
    The script will print, for every line in input, if it found or not a cover.
1. Search manualy URLs of cover images for items on not_found_decitre.csv file:\
    Replace the title by a link to cover image found on internet, for example\
    `271;"Le grand livre des énigmes"`\
    become\
    `271;https://images.epagine.fr/323/9782501105323_1_75.jpg`\
    **Warning** the link must end with a file .jpg ou .png (for example), if it doesn't end with a filename, the script can't download it.
1. Run the script *urls* with the path to created file as arg:\
    `./get_covers_from_urls.sh /home/user/.../urls_file.csv`\
    The script will print, for every line in input, if it found or not a cover.
1. Go back to Koha, in Tools > Upload local cover image
1. Select the "covers_from_decitre.zip" file generated next to the script.
1. Keep checked "ZIP file" and click on "Process images"
1. Select the "covers_from_urls.zip" file generated next to the script.
1. Keep checked "ZIP file" and click on "Process images"
