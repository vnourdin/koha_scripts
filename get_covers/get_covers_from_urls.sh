#! /bin/bash

input_file="$1"

# regex to find the cover filename in the url
filename_regex='([^\/]*\.[a-zA-Z]{3,4})$'

output_dir="covers_from_urls"
mkdir "${output_dir}"

while IFS=";" read -r biblionumber cover_url
do
    if [[ ${cover_url} =~ ${filename_regex} ]]
    then
        filename="${BASH_REMATCH[1]}"
        curl  --silent --output "${output_dir}/${filename}" "${cover_url}"
        echo "found cover for ${biblionumber}"
        echo "${biblionumber},${filename}" >> ${output_dir}/datalink.txt
    else
    	echo "bad url for ${biblionumber}"
        echo "${biblionumber};${cover_url}" >> bad_url.csv
    fi
done < "${input_file}"

zip -qjr "${output_dir}.zip" "${output_dir}"
