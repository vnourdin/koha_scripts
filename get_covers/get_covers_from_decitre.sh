#! /bin/bash

input_file="$1"

# regex to find the cover url in the decitre's html
cover_url_regex='property="og:image" content="([^"]*)"'
# regex to find the cover filename in the url
filename_regex='([^\/]*\.[a-zA-Z]{3,4})$'

output_dir="covers_from_decitre"
mkdir "${output_dir}"

# Remove the first line (CSV header)
sed -i '1d' ${input_file}

while IFS=";" read -r biblionumber isbn title
do
    # get the decitre's html page about this isbn
    content=$(curl --silent -L "https://www.decitre.fr/rechercher/result/index/q/${isbn}")

    if [[ ${content} =~ ${cover_url_regex} ]]
    then
        cover_url="${BASH_REMATCH[1]}"
        if [[ ${cover_url} =~ ${filename_regex} ]]
        then
                filename="${BASH_REMATCH[1]}"
                curl  --silent --output "${output_dir}/${filename}" "${cover_url}"
                echo "found cover for ${title}"
                echo "${biblionumber},${filename}" >> ${output_dir}/datalink.txt
        else
                echo "bad url for ${title}"
                echo "${biblionumber};${isbn};${cover_url}" >> bad_url_decitre.csv
        fi
    else
        echo "cover not found for ${title}"
        echo "${biblionumber};${title}" >> not_found_decitre.csv
    fi
done < "${input_file}"

zip -qjr "${output_dir}.zip" "${output_dir}"
