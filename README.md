# koha_scripts

## FR
Ce dépôt contient des scripts pour le SIGB Koha.\
Pour plus de détails, allez voir dans un dossier.

Ce sont des scripts BASH, il faut donc avoir une machine avec GNU/Linux ou MacOS pour les exécuter.\
C'est également possible sous Windows mais cela demande plus de démarche.

**N'hésitez pas à faire des retours, des issues, des propositions pour améliorer ces scripts !**

## EN
This repo contains scripts relative to Koha ILS.\
For more details, go into a folder.

These are BASH scripts so you need to be under GNU/Linux or MacOS to run them.\
It can work on Windows too but require more work to configure.

**Do not hesitate to give feedbacks, open issues and send me proposals to improve thoses scripts.**